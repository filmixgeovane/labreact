import logo from './cabecalho.png'

export default function Cabecalho(){
  return (
      <img src={logo} alt="Logo do Alura Space" />  )
}