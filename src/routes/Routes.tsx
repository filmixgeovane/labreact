import React from "react";
import { Route, BrowserRouter as Router, Link } from "react-router-dom";
import Home from "../componentes/Home";
import About from "../componentes/About";
import Contact from "../componentes/Contact";

const Routes = () => {
   return (
       <Router>
           <nav>
               <ul>
                   <li>
                       <Link to="/">Home</Link>
                   </li>
                   <li>
                       <Link to="/about">About</Link>
                   </li>
                   <li>
                       <Link to="/contact">Contact</Link>
                   </li>
               </ul>
           </nav>

           <Route path="/" Component={Home} />
           <Route path="/about" Component={About} />
           <Route path="/contact" Component={Contact} />
       </Router>
   );
}

export default Routes;
