import { prototype } from 'events';
import React from 'react';
import { isPropertyAccessOrQualifiedName } from 'typescript';

interface Props {
  value: string,
  id: string,
  name: string
}

function alertar()
{
  console.log('teste de chamar function');
}

const Greet: React.FC<Props> = (props) => (
  <button id={props.id} onClick={alertar}>{props.value}</button>
);

export default Greet;