import { prototype } from 'events';
import React from 'react';
import { isPropertyAccessOrQualifiedName } from 'typescript';
import imagem from './cabecalho.png';

interface Props {
  value: string,
  id: string,
  name: string,
  URL: string
}

function clique() {
    console.log('vou pro google.com');
}

const Greet: React.FC<Props> = (props) => (
  <img src={imagem} onClick={clique}/>
);

export default Greet;